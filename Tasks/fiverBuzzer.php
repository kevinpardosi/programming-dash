<?php

/**
 *
 * @author       Kevin Pardosi <kevingatpardosi@gmail.com>
 * @copyright    Copyright (c) 2019 Kevin Pardosi <kevingatpardosi@gmail.com>
 */
class fiverBuzzer {
    
    /**
     * Print value of iteration
     * 
     * This method will print the value of iteration from starting given number to ending given number
     * 
     * @param int $x starting number for iteration
     * @param int $x ending number for iteration    
     */
    public function printIteration($x, $y): void {
        
        /**
         * starting value for iteration
         * 
         * @var int
         */
        $start = $x;
        
        /**
         * ending value for iteration
         * 
         * @var int
         */
        $end = $y;
        
        /**
         * container variable for current start during iteration
         * 
         * @var int
         */
        $curValue;
        
        //iteration for print the value, and print as string as proviso if it is a specified multiple
        for($start; $start <= $end; $start++){
            $curValue = $start;
            
            if($curValue%3  == 0 && $curValue%5 == 0){
                echo 'FiverBuzzer';            
            } else if($curValue%6 == 0){
                echo 'Buzzer';
            } else if($curValue%5 == 0){
                echo 'Fiver';
            } else {
                echo $curValue;
            }
            
            echo '<br>';
        }        
    }
}
