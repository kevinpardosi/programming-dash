<?php

/**
 * Method for print my understanding of OOP concept
 * 
 * @author       Kevin Pardosi <kevingatpardosi@gmail.com>
 * @copyright    Copyright (c) 2019 Kevin Pardosi <kevingatpardosi@gmail.com>
 */
class oop {

    public function print() {
        
        echo 'Pemahaman saya terkait OOP, OOP teknik yang secara pribadi menurut saya sangat membantu saya dalam membangun aplikasi. '
        . 'Karena saya dituntut membangun aplikasi yang cukup kompleks, konsep ini diterapkan disalah satu framework yang sering saya gunakan yaitu Yii2, '
        . 'pada Yii2 konsep ini dikombinasikan dengan teknik MVC yang semakin membuat struktur kodingan saya lebih terstruktur.'
        . 'Mengapa saya bilang teknik OOP sangat membantu saya? Karena dengan teknik ini aplikasi saya bisa dibangun dengan kodingan yang menurut saya lebih free, '
        . 'karena fungsi-fungsi yang kita buat dibungkus menjadi sebuah object atau class yang bersifat modular, sehingga ketika melakukan perubahan kodingan tidak akan memengaruhi yang lain. '
        . 'Apalagi dengan peng-object-an ini, saya tidak perlu repot mengatur urutan pemograman yang harus sistematis. '
        . 'Ini juga yang membuat saya lebih leluasa menggunakan fungsi-fungsi atau method saya secara berulang.';
    }

}
