<?php

/**
 *
 * @author       Kevin Pardosi <kevingatpardosi@gmail.com>
 * @copyright    Copyright (c) 2019 Kevin Pardosi <kevingatpardosi@gmail.com>
 */
class tweet {

    /**
     * Check if tweet message contain account name
     * 
     * This method will check if the given tweet message is contain the given account name
     * 
     * @param string $accountName name to check
     * @param string $tweetMessage message to be checked 
     * @return bool true if $tweetMessage doesn't contain $accountName
     */
    public function isMentioned($accountName, $tweetMessage) {
        
        /**
         * The account name from param $accountName
         * 
         * @var string
         */
        $account = $accountName;
        
        /**
         * The tweet message from param $tweetMessage
         * 
         * @var string
         */
        $tweet = $tweetMessage;
        
        /**
         * contain status for return
         * 
         * @var bool
         */
        $status = false;
        
        //checking with if statement and regular expression match
        if (preg_match("/@{$account}/i", $tweet)) {
            return $status = true;            
        }
    }

}
