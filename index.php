<!DOCTYPE html>

<?php
    include 'tasks/tweet.php';
    include 'tasks/fiverBuzzer.php';
    include 'tasks/oop.php';
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
            
            echo 'TASK';
            
            //TASK I #oop.php
            echo '<br>' . '<br>' . 'I.' . '<br>';
            
            //initialize object oop from class oop
            $oop = new oop();
            
            //implement method print from object
            $oop->print();
            
            //TASK II #tweet.php
            echo '<br>' . '<br>' . '<br>' . 'II.' . '<br>';
        
            //initialize object tweet from class tweet
            $tweet = new tweet();
            $array = [
                '0' => [
                    '0' => 'azureru',
                    '1' => 'FF @azureru #test #some #stuff',
                ],
                '1' => [
                    '0' => 'akurni',
                    '1' => 'Hello world, this is my first tweet cc: @akurni',
                ],
                '2' => [
                    '0' => null,
                    '1' => 'Tweet without mentioning anything',
                ]
            ];
            
            //Iteration using looping for, and Checking with conditional operator (ternary) for print
            for($i = 0; $i < count($array); $i++){
                
                //implement method isMentioned from object
                $status = $tweet->isMentioned($array[$i][0], $array[$i][1]);
                
                $message = 'TRUE, ' . $array[$i][1] . '<br>';                
                
                echo ($status ? $message : '');
            }
            
            //TASK III #fiverBuzzer.php
            echo '<br>' . '<br>' . 'III.' . '<br>';

            //initialize object fiverBuzzer from class fiverBuzzer
            $fiverBuzzer = new fiverBuzzer();
            
            //implement method printIteration from object
            $fiverBuzzer->printIteration(1, 100);
        ?>
    </body>
</html>
